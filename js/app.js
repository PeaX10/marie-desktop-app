// Reconnaissance vocale
var recognizer = null;
var marieDetected = false;
var transcription = document.getElementById('transcription');
// Synthese vocale
var voices = speechSynthesis.getVoices();
var msg = new SpeechSynthesisUtterance();
var ipc = require('electron').ipcRenderer;
var osascript = require('node-osascript');
var forcestop = false;
var willspeak = false;

function speak(text) {

    msg.text = text;

    msg.volume = 1.;
    msg.rate = 1.;
    msg.pitch = 1.;

    msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name == 'Aurelie'; })[0];
    msg.lang = 'fr-FR';

    msg.onstart = function(event){

    };
    msg.onend = function(event){
      ipc.send('MarieOff', true);
      recognizer.start();
      willspeak = false;
    };

    speechSynthesis.speak(msg);
    console.log(msg);
}

function recognize()
{
    // Récupération du moteur de reconnaissance vocale
    window.speechRecognition = window.speechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.webkitSpeechRecognition;

    if(window.speechRecognition == undefined)
    {
        alert("La reconnaissance vocale n'est pas supporté");
    }
    else
    {
        recognizer = new speechRecognition();

        // Recognizer Options
        recognizer.continuous = true;
        recognizer.lang = "fr-FR";
        recognizer.interimResults = true;

        // Reconnaissance vocale activé
        recognizer.onstart = function(){
            recOn();
        }

        // Quand la reconnaissance de Marie est coupé
        recognizer.onend = function(){
            recOff();
            if(!forcestop){
              if(marieDetected == false){
                  recognizer.continuous = true;
                  recognizer.interimResults = true;
                  // On veut savoir si il va parler
                  if(speechSynthesis.pending == false || speechSynthesis.speaking == false){
                      // Si il a fini ou ne vas pas parler on relance la reconnaissance du mot Marie
                      recognizer.start();
                  }

              }else{
                  ipc.send('MarieOn', true);
                  marieDetected = false;
                  recognizer.continuous = false;
                  recognizer.interimResults = false;
                  recognizer.start();
                  StartMic();
              }
            }
        }

        // Analyse des résultats
        recognizer.onresult = function(event){
            //event.resultIndex returns the index of first word spoken in the currently stoped sentence.
            //event.results.length is the total number of words spoken in this session.
            for(var count = event.resultIndex; count < event.results.length; count++){
                if(Marie(event.results[count][0].transcript) && recognizer.interimResults == true){
                    marieDetected = true;
                    recognizer.stop();
                }else if(recognizer.interimResults == false){
                    transcription.textContent = event.results[count][0].transcript;
                    $.ajax({
                        method: "GET",
                        url: "http://marie.dev/ia/" + encodeURIComponent(event.results[count][0].transcript)
                    }).done(function( msg ) {
                        recognizer.stop();
                        msg = jQuery.parseJSON(msg);
                        // On traite les ordres 1 par 1
                        $.each(msg, function (key, value) {
                          // Dans le futur enlever cette deuxieme boucle et remplacer value = value[0] pour mieux récuperer params
                          $.each(value, function (type, result) {
                            if(type == 'text'){
                              transcription.textContent = result;
                              speak(result);
                              willspeak = true;
                            }else if(type != 'params'){
                              // On appelle la fonction qui va traiter les actions en fonction de l'OS
                              params = [];
                              if("params" in value) params = value['params'][0];
                              var OS = detectOS();
                              switch(OS){
                                case 'Windows':
                                  winAction(type, result, params);
                                  break;
                                case 'MacOS':
                                  macAction(type, result, params);
                                  break;
                                case 'Linux':
                                  linuxAction(type, result, params);
                              }
                            }
                          });
                        });
                        StopMic();
                    });
                }else{
                    // DEBUG
                    //console.log(event.results[count][0].transcript);
                }
            }
        }

        recognizer.onerror = function(event){
            //alert('Speech recognition error detected: ' + event.error);
            //alert('Additional information: ' + event.message);
        }

        recognizer.start();
    }
}

// Sons
ion.sound({
    sounds: [
        {
            name: "start"
        },
        {
            name: "end"
        }
    ],
    volume: 1,
    path: "sound/",
    preload: true
});

function stopRecognize(){
    recognizer.stop();
}

function Marie(str){
    str = str.toLowerCase();
    if((str.indexOf("marie") >= 0) || (str.indexOf("mari") >= 0)|| (str.indexOf("mary") >= 0)){
        return true;
    }else{
        return false;
    }
}

function StartMic(){
    $('#mic').attr('src', './img/mic.gif');
    $('#mic').addClass('animated');
    $('#mic').removeClass('not-animated');
    $('#transcription').text('...');
    ion.sound.play("start");
}

function StopMic(queue){
    $('#mic').attr('src', './img/mic.png');
    $('#mic').addClass('not-animated');
    $('#mic').removeClass('animated');
    if(queue) window.speechSynthesis.cancel();
    ion.sound.play("end");
    if(willspeak == false){
      ipc.send('MarieOff', true);
      recognizer.start();
    }
}

function recOn(){
    $('#rec-state').attr('src', './img/rec-on.png');
}

function recOff(){
    $('#rec-state').attr('src', './img/rec-off.png');
}

function detectOS(){
  var OSName= "Unknown OS";
  if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
  if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
  if (navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
  if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";
  return OSName;
}

function macAction(type, action, params){
  switch(type){
    case 'spotify':
      switch(action['action']){
        case 'play':
          ipc.send('osascript', 'tell app "Spotify" to play');
        break;
        case 'pause':
          ipc.send('osascript', 'tell app "Spotify" to pause');
        break;
        case 'next track':
          ipc.send('osascript', 'tell app "Spotify" to next track');
        break;
        case 'previous track':
          ipc.send('osascript', 'tell app "Spotify" to previous track');
          ipc.send('osascript', 'tell app "Spotify" to previous track');
        break;
        case 'reset':
          ipc.send('osascript', 'tell app "Spotify" to previous track');
        break;
        case 'sound':
          ipc.send('osascript', 'tell app "Spotify" set sound volume to '+params['sound']);
        break;
      }
    break;
    case 'app':
      ipc.send('appCommand', action);
      break;
    case 'o-app':
      switch(action['action']){
        case 'open':
          ipc.send('osascript', 'tell app "'+ params['app'] +'" to activate');
          ipc.send('osascript', 'tell application "Finder" to set visible of process "'+ params['app'] +'" to true');
        break;
        case 'close':
          ipc.send('osascript', 'quit app "'+ params['app'] +'"');
        break;
      }
      break;
  }
}

function winAction(type, result){
  // A compléter
}

function linuxAction(type, result){
  // A compléter
}

$('#mic').click(function(){
    if($('#mic').hasClass('not-animated')){
        marieDetected = true;
        recognizer.stop();
        StartMic();
    }else{
        stopRecognize();
        StopMic();
    }
});

speak('Bienvenue...');

recognize();
