const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const dialog = electron.dialog
const globalShortcut = electron.globalShortcut
var ipc = require('electron').ipcMain
var osascript = require('node-osascript')

let mainWindow

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function(){
  var electronScreen = electron.screen;
  var size = electronScreen.getPrimaryDisplay().workAreaSize;

  mainWindow = new BrowserWindow({width: size.width, height: size.height, icon: './img/icon.png', transparent: true, title: "MARIE", frame: false, resizable: false, show: false})
  mainWindow.maximize()
  mainWindow.loadURL('file://' + __dirname + '/index.html')
  mainWindow.hide()
  globalShortcut.register('CommandOrControl+Enter', function () {
    if(mainWindow.isVisible()){
      mainWindow.hide()
    }else{
      mainWindow.show()
    }
  })
  mainWindow.webContents.session.clearCache(function(){
//some callback.
});
})

ipc.on('MarieOn', function(event, data){
    mainWindow.show()
});

ipc.on('MarieOff', function(event, data){
    mainWindow.hide()
});

ipc.on('osascript', (event, arg) => {
  osascript.execute(arg, function(err, result, raw){
    if (err) return console.error(err)
    console.log(result, raw)
  })
  console.log(arg)
})

app.on('will-quit', function () {
  globalShortcut.unregisterAll()
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

ipc.on('appCommand', function(event, data){
  switch(data['action']){
    case 'show':
      mainWindow.show()
      break
    case 'hide':
      mainWindow.hide()
      break
  }
});
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
